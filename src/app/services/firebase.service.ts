import { Injectable } from '@angular/core'
import { AngularFirestore } from '@angular/fire/firestore'
import { AngularFireAuth } from '@angular/fire/auth'
import * as firebase from 'firebase/app'
import 'firebase/storage'

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  private snapshotChangesSubscription: any;

  constructor(
    public angularFirestore: AngularFirestore,
    public angularFireAuth: AngularFireAuth
  ) { }

  getTasks() {
    return new Promise<any>((resolve, reject) => {
      this.angularFireAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.angularFirestore
            .collection('users')
            .doc(currentUser.uid)
            .collection('tasks')
            .snapshotChanges()

          resolve(this.snapshotChangesSubscription)
        }
      })
    })
  }

  getTask(taskId: string) {
    return new Promise<any>((resolve, reject) => {
      this.angularFireAuth.user.subscribe(currentUser => {
        if (currentUser) {
          this.snapshotChangesSubscription = this.angularFirestore
            .collection('users').doc(currentUser.uid)
            .collection('tasks').doc(taskId)
            .valueChanges()
            .subscribe(snapshots => {
              resolve(snapshots)
            }, err => {
              reject(err)
            })
        }
      })
    })
  }

  createTask(value: { title: string; description: string; image: any; }) {
    return new Promise<any>((resolve, reject) => {
      const currentUser = firebase.auth().currentUser;

      const task = {
        title: value.title,
        description: value.description,
        image: value.image
      }

      this.angularFirestore
        .collection('users')
        .doc(currentUser.uid)
        .collection('tasks')
        .add(task)
        .then(res => resolve(res), err => reject(err))
    })
  }

  updateTask(taskKey: string, value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;

      this.angularFirestore
        .collection('users').doc(currentUser.uid)
        .collection('tasks').doc(taskKey)
        .set(value)
        .then(res => resolve(res), err => reject(err))
    })
  }

  deleteTask(taskKey: string) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;

      this.angularFirestore
        .collection('users').doc(currentUser.uid)
        .collection('tasks').doc(taskKey)
        .delete()
        .then(res => resolve(res), err => reject(err))
    })
  }

  uploadImage(imageURI: string, randomId: string) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(randomId);

      this.encodeImageUri(imageURI, (image64: string) => {
        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            snapshot.ref
              .getDownloadURL()
              .then(res => resolve(res))
          })
          .catch(reject)
      })
    })
  }

  encodeImageUri(imageUri: string, callback: { (image64: string): void; }) {
    let canvas = document.createElement('canvas')
    let context = canvas.getContext("2d")
    let image = new Image()

    image.onload = function () {
      let aux: any = this

      canvas.width = aux.width
      canvas.height = aux.height

      context.drawImage(image, 0, 0)

      const dataURL = canvas.toDataURL("image/jpeg")

      callback(dataURL)
    }

    image.src = imageUri
  }
}

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBcUHMJHgLKDhOzj_RAVoAkp0JKEWay0IU",
    authDomain: "ns-todolist.firebaseapp.com",
    databaseURL: "https://ns-todolist.firebaseio.com",
    projectId: "ns-todolist",
    storageBucket: "ns-todolist.appspot.com",
    messagingSenderId: "625963402900",
    appId: "1:625963402900:web:06ccc453c3163eae"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

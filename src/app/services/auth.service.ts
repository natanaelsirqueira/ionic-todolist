import { Injectable } from '@angular/core'
import * as firebase from 'firebase/app'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  register({ email, password }: { email: string, password: string }) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth()
        .createUserWithEmailAndPassword(email, password)
        .then(res => resolve(res), err => reject(err))
    })
  }

  login({ email, password }: { email: string, password: string }) {
    return new Promise((resolve, reject) => {
      firebase.auth()
        .signInWithEmailAndPassword(email, password)
        .then(res => resolve(res), err => reject(err))
    })
  }

  logout() {
    return new Promise(() => {
      firebase.auth().signOut()
    })
  }
}

import { Component, OnInit } from '@angular/core'
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms'
import { LoadingController, ToastController } from '@ionic/angular'
import { Router } from '@angular/router'

import { ImagePicker } from '@ionic-native/image-picker/ngx'
import { WebView } from '@ionic-native/ionic-webview/ngx'

import { FirebaseService } from '../../services/firebase.service'

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.page.html',
  styleUrls: ['./new-task.page.scss'],
})
export class NewTaskPage implements OnInit {

  validations_form: FormGroup
  image: any

  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private firebaseService: FirebaseService,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private imagePicker: ImagePicker,
    private webview: WebView
  ) { }

  ngOnInit() {
    this.resetFields()
  }

  resetFields() {
    this.image = "./assets/imgs/default_image.jpg"

    this.validations_form = this.formBuilder.group({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    })
  }

  onSubmit(value: { title: string; description: string; }) {
    let data = {
      title: value.title,
      description: value.description,
      image: this.image
    }

    this.firebaseService
      .createTask(data)
      .then(() => this.router.navigate(["/home"]))
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then(result => {
        if (result) {
          this.imagePicker
            .getPictures({ maximumImagesCount: 1 })
            .then(res => {
              for (var i = 0; i < res.length; i++) {
                this.uploadImageToFirebase(res[i])
              }
            })
            .catch(console.log)
        } else {
          this.imagePicker.requestReadPermission()
        }
      })
      .catch(console.log)
  }

  async uploadImageToFirebase(image: string) {
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    })

    const toast = await this.toastCtrl.create({
      message: 'Image was updated successfully',
      duration: 3000
    })

    this.presentLoading(loading)

    let imageSrc = this.webview.convertFileSrc(image)
    let randomId = Math.random().toString(36).substr(2, 5)

    this.firebaseService
      .uploadImage(imageSrc, randomId)
      .then(photoURL => {
        this.image = photoURL
        loading.dismiss()
        toast.present()
      })
      .catch(console.log)
  }

  async presentLoading(loading: HTMLIonLoadingElement) {
    return await loading.present()
  }
}

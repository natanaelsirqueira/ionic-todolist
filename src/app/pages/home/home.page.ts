import { Component } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

import { LoadingController } from '@ionic/angular'

import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  items: Array<any>

  constructor(
    public loadingCtrl: LoadingController,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData()
    }
  }

  async getData() {
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    })

    this.presentLoading(loading)

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe((data: any[]) => {
        loading.dismiss()

        this.items = data
      })
    })
  }

  async presentLoading(loading: HTMLIonLoadingElement) {
    return await loading.present()
  }

  logout() {
    this.authService.logout()
      .then(() => this.router.navigate(["/login"]))
      .catch(console.log)
  }
}

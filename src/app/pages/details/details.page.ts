import { Component, OnInit } from '@angular/core'
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms'
import { LoadingController, ToastController, AlertController } from '@ionic/angular'
import { ImagePicker } from '@ionic-native/image-picker/ngx'
import { WebView } from '@ionic-native/ionic-webview/ngx'
import { ActivatedRoute, Router } from '@angular/router'
import { FirebaseService } from '../../services/firebase.service'

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  validations_form: FormGroup
  image: any
  item: any
  load: boolean = false

  constructor(
    private imagePicker: ImagePicker,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private firebaseService: FirebaseService,
    private webview: WebView,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getData()
  }

  getData() {
    this.route.data.subscribe(routeData => {
      let data = routeData['data']

      if (data) {
        this.item = data
        this.image = this.item.image
      }
    })

    this.validations_form = this.formBuilder.group({
      title: new FormControl(this.item.title, Validators.required),
      description: new FormControl(this.item.description, Validators.required)
    })
  }

  onSubmit(value: { title: any; description: any; }) {
    let data = {
      title: value.title,
      description: value.description,
      image: this.image
    }

    this.firebaseService
      .updateTask(this.item.id, data)
      .then(() => this.router.navigate(["/home"]))
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm',
      message: 'Do you want to delete ' + this.item.title + '?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => { }
        },
        {
          text: 'Yes',
          handler: () => {
            this.firebaseService
              .deleteTask(this.item.id)
              .then(
                res => this.router.navigate(["/home"]),
                err => console.log(err)
              )
          }
        }
      ]
    })

    await alert.present()
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission()
      .then(result => {
        if (result) {
          this.imagePicker
            .getPictures({ maximumImagesCount: 1 })
            .then(res => {
              for (var i = 0; i < res.length; i++) {
                this.uploadImageToFirebase(res[i])
              }
            })
            .catch(console.log)
        } else {
          this.imagePicker.requestReadPermission()
        }
      })
      .catch(console.log)
  }

  async uploadImageToFirebase(image: string) {
    const loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    })

    const toast = await this.toastCtrl.create({
      message: 'Image was updated successfully',
      duration: 3000
    })

    this.presentLoading(loading)

    let imageSrc = this.webview.convertFileSrc(image)
    let randomId = Math.random().toString(36).substr(2, 5)

    this.firebaseService
      .uploadImage(imageSrc, randomId)
      .then(photoURL => {
        this.image = photoURL
        loading.dismiss()
        toast.present()
      })
      .catch(console.log)
  }

  async presentLoading(loading: HTMLIonLoadingElement) {
    return await loading.present()
  }
}

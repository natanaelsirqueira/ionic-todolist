import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public angularFireAuth: AngularFireAuth
  ) {
    this.initializeApp()
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.angularFireAuth.user.subscribe({
        next: (user) => {
          if (user) {
            this.router.navigate(["/home"])
          } else {
            this.router.navigate(["/login"])
          }
        },
        error: () => this.router.navigate(["/login"]),
        complete: () => this.splashScreen.hide()
      })

      this.statusBar.styleDefault()
    })
  }
}
